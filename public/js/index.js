function init() {
    
    var user_email = '';
    
    if(Notification.permission=== "default"){
        Notification.requestPermission();
    }
    
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='profile-btn'>"+user_email+"</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
 
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var username=document.getElementById('name');
            var Ref = firebase.database().ref('com_list');
            var data = {
                data: post_txt.value,
                name1: username.value,
                email: user_email,
                time: getTime()
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });

    // The html code for post    
    var str_before_username = "<div class='row mb-2'><div class='card flex-md-row mb-4 box-shadow h-md-250'><div class='card-body d-flex flex-column align-items-start'><h6 class='border-bottom border-gray pb-2 mb-0'></h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div></div></div>\n";

    var str_before_username_me = "<div class='row mb-2'><div class='col-md-6'><div class='card-body d-flex flex-column align-items-start'></div></div><div class='card flex-md-row mb-4 box-shadow h-md-250'><div class='card-body d-flex flex-column align-items-start'><h6 class='border-bottom border-gray pb-2 mb-0'></h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    
    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            var del_btn = document.getElementsByClassName('btn btn-danger');
            snapshot.forEach(function(childshot){
                var data = childshot.val();
                if(user_email!=data.email){
                    total_post[total_post.length] = str_before_username + data.name1 + " " + data.email + " " + data.time + "</strong>" + data.data + str_after_content;
                    first_count += 1
                }
                else{
                    total_post[total_post.length] = str_before_username_me + data.name1 + " " + data.email + " " + data.time + "</strong>" + data.data + str_after_content;
                    first_count += 1
                }
            });

            
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(user_email!=data.email){
                        total_post[total_post.length] = str_before_username + childData.name1 + " " + childData.email + " " + childData.time + "</strong>" + childData.data + str_after_content
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    else{
                        total_post[total_post.length] = str_before_username_me + childData.name1 + " " + childData.email + " " + childData.time + "</strong>" + childData.data + str_after_content
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                    if(user_email!=childData.email&&Notification.permission==="granted"){
                        var string = childData.name1 + "剛剛說話囉!" 
                        var notification=new Notification(string);
                    }
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};

function getTime() {
    var date = new Date();
    var h = date.getHours();
    var m = date.getMinutes();
    var s = date.getSeconds();
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }
    var now = h + ':' + m + ':' + s;
    return now;
  }
