# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer


## Topic
* [Chat room]
* Key functions (add/delete)
    1. [init] chat
    2. [postsRef.once('value').then(function (snapshot)] load message history
    3. [xxx]
    4. [xxx]
* Other functions (add/delete)
    1. [getTime] post time
    2. [notification] notification
    3. [username] change username
    4. [Css animation]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description

https://simple-forum-105062137.firebaseapp.com
https://gitlab.com/105062137/Midterm_Project/blob/master/README.md

比較特別的功能有顯示上傳時間,更改暱稱,背景動畫,有人說話時會跳Chrome的通知
上傳時間是用getTime來抓按下submit的時間,再上傳到database上,更改暱稱也是用類似的方法,在data裡新增username再修改,而通知則是在second_count時判斷,如果database裡的貼文username不是自己,就跳出通知
## Security Report (Optional)


